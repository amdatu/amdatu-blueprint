#
# Amdatu Blueprint email feature
#
blueprint-feature.email: email
blueprint-repo.email: https://repository.amdatu.org/amdatu-email/r8/repo/index.xml.gz
blueprint-deps.email: \
	com.fasterxml.jackson.core:jackson-annotations:${com-fasterxml-jackson-core-version},\
	com.fasterxml.jackson.core:jackson-core:${com-fasterxml-jackson-core-version},\
	com.fasterxml.jackson.core:jackson-databind:${com-fasterxml-jackson-core-version},\
	commons-codec:commons-codec:${commons-codec-version},\
	org.apache.httpcomponents:httpclient-osgi:${org-apache-httpcomponents-httpclient-osgi-version},\
	org.apache.httpcomponents:httpcore-osgi:${org-apache-httpcomponents-httpcore-osgi-version}

#
# Build
#
-buildpath.blueprint-email: \
	${if;(buildfeaturesMerged[]=email); \
		org.amdatu.email.api\
	}

#
# Run
#
-runbundles.blueprint-email: \
	${if;(runfeaturesMerged[]=email); \
		org.amdatu.email.impl\
	}

# Select transport based on 'email-transport' value defaults to 'mock'
blueprint-email-transport: ${def;email-transport;mock}

# Mock Transport
-runbundles.blueprint-email-mock: \
	${if;(&(runfeaturesMerged[]=email)(blueprint-email-transport[]=mock)); \
		org.amdatu.email.mock\
	}

# SMTP Transport
-runbundles.blueprint-email-smtp: \
	${if;(&(runfeaturesMerged[]=email)(blueprint-email-transport[]=smtp)); \
		org.amdatu.email.smtp\
	}

-runsystempackages.blueprint-email-smtp: \
	${if;(&(runfeaturesMerged[]=email)(blueprint-email-transport[]=smtp)); \
		sun.security.util\
	}