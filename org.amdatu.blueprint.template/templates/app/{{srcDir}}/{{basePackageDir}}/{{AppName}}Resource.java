package {{basePackageName}};

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

@Component(provides = Object.class)
@Property(name = JaxrsWhiteboardConstants.JAX_RS_RESOURCE, booleanValue = true)
@Property(name = JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT, value = {{AppName}}App.JAX_RS_APPLICATION_NAME)
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class {{AppName}}Resource {


}
