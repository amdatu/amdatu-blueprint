package {{basePackageName}};

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;


@Component(provides = ServletContextHelper.class)
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME, value = {{AppName}}App.CONTEXT_NAME)
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH, value = "{{httpContextPath}}")
public class {{AppName}}ServletContextHelper implements ServiceFactory<ServletContextHelper> {

    @Override
    public ServletContextHelper getService(Bundle bundle, ServiceRegistration<ServletContextHelper> registration) {
        return new ServletContextHelper(bundle) {};
    }

    @Override
    public void ungetService(Bundle bundle, ServiceRegistration<ServletContextHelper> registration,
        ServletContextHelper service) {
        // nothing here
    }

}
