== Resources

[cols="40%,60%"]
|===
| Tools
	| Location
| Source Code
	| https://bitbucket.org/amdatu/amdatu-blueprint[^]
| Issue Tracking
	| https://amdatu.atlassian.net/browse/AMDATUBP[^]
| Continuous Build
	| https://bitbucket.org/amdatu/amdatu-blueprint/addon/pipelines/home[^]
|===
