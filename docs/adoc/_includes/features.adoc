:source-highlighter: coderay
:icons: font

= Features

Amdatu Blueprint provides features for common things,


== Base

The base feature provides basic infrastructural services used in almost all projects like logging, dependency management and testing libraries.

[TIP]
====
Because libraries provided by the base feature are commonly used the feature is enabled by default.
====

== Blobstores

The blobstores feature adds support for storing files in the cloud using Amdatu Blobstores.

For more information: https://amdatu.org/components/amdatu-blobstores/[Amdatu Blobstores project documentation^]

== Email

With the email feature you get support for sending email, this is backed by Amdatu Email.
Based on runtime configuration the mail can be sent using SMTP, Amazon Simple Email Service or for development to a mock service that just prints the email to the console.

=== Configuration

[cols='5%,20%,75%']
|===
2+|`email-transport`
    | Run feature option to select email transport, defaults to *mock*
|
    | mock
    | Use mock email transport, this will print te email to the console
|
    | aws
    | Use aws email transport, this will send mail using Amazon SES
|
    | smtp
    | Use smtp email transport, this will send mail using SMTP
|===

For more information: https://amdatu.org/components/amdatu-email/[Amdatu Email project documentation^]

== Mongodb

With the mongodb feature you get support for Mongodb, this is backed by Amdatu Mongodb.

For more information: https://amdatu.org/components/amdatu-mongodb[Amdatu Mongodb project documentation^]

== Scheduling

With the scheduling feature you get support for running scheduled tasks, this is backed by Amdatu Scheduling.

For more information: https://amdatu.org/components/amdatu-scheduling/[Amdatu Scheduling project documentation^]

== Shell

With the scheduling feature you get the gogo shell.

For more information: http://felix.apache.org/documentation/subprojects/apache-felix-gogo.html[Apache Felix gogo documentation^]

== Security

With the security feature you get support for building secure web applications, this is backed by Amdatu Security.

=== Configuration

[cols='5%,20%,75%']
|===
2+|`security-account-admin`
    | Feature option used to enable the Amdatu Security account admin component.
|
    | true
    | Enabled
|
    | false (default)
    | Disabled
|===


For more information: https://amdatu.org/components/amdatu-security/[Amdatu Security project documentation^]

// Hmm maybe we should leave this one out... there are projects using it but it's not "the best feature"
//== Templatedocs/index.html
//
//For more information: https://amdatu.org/components/amdatu-template/[Amdatu Template project documentation^]

== Testing

With the testing feature you get support for integration test projects, this is backed by Amdatu Testing.
When used in combination with the mongodb feature additional support for testing with Mongodb is automatically added.
When used in combination with the web feature additional support for testing web applications / services is automatically added.

For more information: https://amdatu.org/components/amdatu-testing/[Amdatu Testing project documentation^]

== Validator

With the validator feature you get support for bean validation, this is backed by Amdatu Validator.

For more information: https://amdatu.org/components/amdatu-validator/[Amdatu Validator project documentation^]

== Web

With the web feature you get support for writing web applications and services, this is backed by Amdatu Web.

For more information: https://amdatu.org/components/amdatu-web/[Amdatu Web project documentation^]
